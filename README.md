meta-spec7
==========

This file contains information on the contents of the meta-spec7 layer.

Please see the corresponding sections below for details.

Checkout "thud" branch before building.

Dependencies
============
```
URI: git://git.openembedded.org/openembedded-core 
layers: meta
branch: master


URI: git://git.openembedded.org/meta-openembedded
layers: meta-oe
branch: thud

URI: git://git.yoctoproject.org/meta-xilinx
layers: meta-xilinx
branch:thud

URI: git://git.yoctoproject.org/meta-xilinx-tools
layers: meta-xilinx-tools
branch:master
```


I. Adding the meta-spec7 layer to your build
=================================================

```
$ cd poky/
$ git clone https://gitlab.cern.ch/mshukla/meta-spec7.git
```

Run 'bitbake-layers add-layer meta-spec7'

Run 'bitbake core-image-minimal'

II. Images
========
The images after the yocto build are available here- build/tmp/deploy/images/zc706-zynq7/

III.Linux
========
The recipe for linux kernel fetches source from - https://gitlab.cern.ch/coht/linux.git<br/>
                                                  checkout=linux-4.14-y